export interface VXETablePluginElementOptions {

}

/**
 * vxe-table renderer plugins for element-ui.
 */
declare var VXETablePluginElement: VXETablePluginElementOptions;

export default VXETablePluginElement;